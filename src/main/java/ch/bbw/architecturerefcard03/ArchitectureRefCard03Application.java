package ch.bbw.architecturerefcard03;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

// Anleitung
// https://learn.microsoft.com/en-us/azure/developer/java/spring-framework/configure-spring-data-jdbc-with-azure-sql-server
@SpringBootApplication
public class ArchitectureRefCard03Application {

    public static void main(String[] args) {
        SpringApplication.run(ArchitectureRefCard03Application.class, args);
    }

}
